// Create Fibonacci

function createFibo(n) {
    if (n < 1) {
        return [];
    } else if (n === 1) {
        return [0]
    } else {
        let arry = [0, 1]
        for (let i = 2; i < n; i++) {
            arry[i] = arry[i - 1] + arry[i - 2];
        }
        return arry;
    }
};

console.log(createFibo(7));