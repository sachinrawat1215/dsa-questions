// Get Factorial

// function getFactorial(n) {
//     let factorial = [1];
//     for (let i = 1; i < n + 1; i++) {
//         factorial[i] = i * factorial[i - 1];
//     }
//     return factorial[n];
// };

function getFactorial(n) {
    if (n === 0 || n === 1 || n < 0) {
        return 1
    } else {
        let factorial = 1;
        for (let i = 1; i < n + 1; i++) {
            factorial = factorial * i;
        }
        return factorial;
    }
}

console.log(getFactorial(0));
console.log(getFactorial(5));
console.log(getFactorial(8));
console.log(getFactorial(12));

// BigO = O(n)